package org.beetl.sql.core.concat;

public class TrimSupport {
    protected boolean trim;

    public boolean isTrim() {
        return trim;
    }

    public void setTrim(boolean trim) {
        this.trim = trim;
    }
}
